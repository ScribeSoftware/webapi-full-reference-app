/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling org APIs
*/


/* Get a list of child orgs
   Details: this function retrieves a list of child orgs under the configured parent org
   and sets the results into a list item html control.
*/
function getChildOrgs() {
    console.log('getting child orgs for id: ' + PARENTORGID);

    $.ajax({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        data: { "parentId": PARENTORGID },
        dateType: 'json',
        success: function (result) {

            //  Now build the table html for the org information
            var orgRows = $.map(result, function (orgItem, index) {
                var tblRow = $('<tr></tr>');
                $('<td>' + orgItem.name + '</td>').appendTo(tblRow);
                $('<td>' + orgItem.id + '</td>').appendTo(tblRow);
                return tblRow;
            });

            // Show the table of org and solution information
            $("#tbl-orgs tbody").html(orgRows);
            $('#divOrgResults').slideDown();
        },
        error: function (xhr, error) {
            console.log(xhr);
            console.log(error);
        }
    });
}

/* Get a list of child orgs and show in a listbox for selection
   This function supports being called from multiple tabs based on the containerId
*/
function getChildOrgsForSelection(containerId) {
    console.log('getting child orgs for id: ' + PARENTORGID);
    $('#' + containerId).find('#select-org-wait').show();

    $.ajax({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        data: { "parentId": PARENTORGID },
        dateType: 'json',
        success: function (result) {

            // Create an html option for each map entry found.
            var items = $.map(result, function (orgItem, index) {
                var optionItem = $('<option value="' + orgItem.id + '">' + orgItem.name + '</option>')
                return optionItem;
            });

            var selectOrg = $('<select id="selectChildOrg" size="8" style="width: 300px;"></select>');
            selectOrg.append(items);

            // Show the list of orgs
            $('#' + containerId).find('#divSelectChildOrg').html(selectOrg);

            $('#' + containerId).find('#select-org-wait').hide();

            // Wire up the click handler for this list box that is showing the orgs to show the solutions for the select org
            var solutionContainerId;
            if (containerId == 'divSelectOrgForMapping')
                solutionContainerId = 'divSelectSolutionForMapping';
            else if (containerId == 'divSelectOrgForMonitoring')
                solutionContainerId = 'divSelectSolutionForMonitoring';

            $('#' + containerId).find('#selectChildOrg').on('click', function () {
                getSolutionsForChildOrg(containerId, solutionContainerId);
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            $('#' + containerId).find('#select-org-wait').hide();

            var responseInfo = $('<p>Error getting the child orgs for parent org: ' + PARENTORGID + ' - ' + errorThrown + '</p>');
            var divFailure = $('#' + containerId).find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Get the cloud agent for the child org 
 */
function getCloudAgentForOrg() {
    console.log('Getting agent info for orgid: ' + orgId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/agents',
        // GET /v1/orgs/{orgId}/agents
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            // Determine if the agent is up and running
            if (result.length > 0) {
                console.log(result);
                // Store the agentId in global variable
                agentId = result[0].id;
                console.log('retrieved cloud agentId: ' + agentId + ' for child orgId: ' + orgId);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
        }
    });
}


/* Create a child organization
   Details: calls the API to create a new child org under the configured parent org.  Saves the api token and
   org id for later use.  Sets a timeout callback to install the connectors (Eloqua and Salesforce) for the
   new org.
*/
function createChildOrg() {

    // Validate that a valid name was supplied
    if ($('#child-org-name').val().length == 0) {
        alert('You must enter an organization name.');
        return;
    }

    $('#create-org-wait').show();

    // Create the object for the new org, taking the name from the input text box
    var childOrg = {};
    childOrg.name = $('#child-org-name').val();
    childOrg.parentId = PARENTORGID;

    console.log('Creating child org...');
    console.log(childOrg);

    $.ajax({
        type: "POST",
        url: APIURL,
        // POST /v1/orgs
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        data: childOrg,
        dateType: 'json',
        success: function (response) {
            // Save the org api token and id for later use
            orgApiToken = response.apiToken;
            orgId = response.id;

            console.log(response);
            var msg = 'Successfully created a child organization with id: ' + orgId + ' and apiToken: ' + response.apiToken;
            console.log(msg);

            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateChildOrg').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Now create the connectors for this org
            setTimeout(installConnectors, 200);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            console.log(errorThrown);

            var responseInfo = $('<p>Error creating the child organization - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateChildOrg').find('.alert-danger')
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Provision the 6 connectors and the agent for a new org
   Details: installs the EloquaBulk, SalesforceBulk, Marketo, SugarCRM, Sample CRM, and Sample Marketing connectors for the new org.  
   If successfull, Sets a timeout callback to provision the agent.
*/
function installConnectors() {

    // Build the ajax calls
    var jqxhrConn1 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + SAMPLE_CRM_CONNECTORID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });
    var jqxhrConn2 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + SAMPLE_MARKETING_CONNECTORID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });
    // var jqxhrConn3 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + ELOQUA_BULK_CONNECTOR_ID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });
    // var jqxhrConn4 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + SALESFORCE_CONNECTOR_ID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });
    // var jqxhrConn5 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + SUGARCRM_CONNECTORID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });
    // var jqxhrConn6 = $.ajax({ type: "POST", url: APIURL + orgId + '/connectors/' + MARKETO_REST_CONNECTORID + '/install', headers: { "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD) }, dateType: 'json' });

    // Execute the ajax calls to install all of the connectors
    //$.when(jqxhrConn1, jqxhrConn2, jqxhrConn3, jqxhrConn4, jqxhrConn5, jqxhrConn6).then(handleSuccessfulConnectorInstalls, handleFailedConnectorInstalls);
    $.when(jqxhrConn1, jqxhrConn2).then(handleSuccessfulConnectorInstalls, handleFailedConnectorInstalls);
}

/* Handler for successful calls to install all connectors
*/
function handleSuccessfulConnectorInstalls(data, statusText) {
    console.log('Successfully installed the connectors for the org.');

    var responseInfo = $('<p>Successfully installed the Sample CRM and Sample Marketing connectors for org: ' + orgId + '</p>');
    //var responseInfo = $('<p>Successfully installed the EloquaBulk, SalesforceBulk, Sugar CRM, Marketo, Sample CRM, and Sample Marketing connectors for org: ' + orgId + '</p>');
    var divSuccess = $('#divCreateChildOrg').find('.alert-success');
    divSuccess.append(responseInfo);

    $('#create-org-wait').hide();

    // Now provision the cloud agent for this org
    setTimeout(provisionCloudAgent, 100);
}

/* Handler for failure during ajax calls to install the connectors
*/
function handleFailedConnectorInstalls(data, statusText) {
    console.log('Error installing one of the connectors for the org.', data.status);
    console.log(data);

    var responseInfo = $('<p>Error installing one of the connectors for orgId: ' + orgId + '</p>');
    var divFailure = $('#divCreateChildOrg').find('.alert-danger')
    divFailure.append(responseInfo);
    divFailure.slideDown();

    $('#create-org-wait').hide();
}

/* Provisions the cloud agent
   Details: provisions the cloud agent for the new org.  If successfull, Sets a timeout callback to 
   get the cloud agent status.
*/
function provisionCloudAgent() {
    console.log('Provisioning cloud agent for orgid: ' + orgId);

    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/agents/provision_cloud_agent',
        // POST /v1/orgs/{orgId}/agents/provision_cloud_agent
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            var msg = 'Successfully provisionsed the cloud agent for org: ' + orgId;
            console.log(msg);
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divCreateChildOrg').find('.alert-success');
            divSuccess.append(responseInfo);

            // Now get back the agent.  Need to pause a bit before the agent is ready
            setTimeout(getCloudAgentInfo, 2500);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error provisioning the agent for the new org - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateChildOrg').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#create-org-wait').hide();
        }
    });
}

/* Get the status for the cloud agent that was just provisioned
   Details: if the status is set to 'Running', then sets a timeout callback
   to show the solutions.
*/
function getCloudAgentInfo() {
    console.log('Getting agent info for orgid: ' + orgId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/agents',
        // GET /v1/orgs/{orgId}/agents
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log('Retrieved agent information for org: ' + orgId);

            // Determine if the agent is up and running
            if (result.length > 0 && result[0].status == 'Running') {
                console.log(result);
                // Remember the agent Id in a global variable for multiple uses later on
                agentId = result[0].id;

                var msg = 'Cloud agent with id: ' + agentId + ' has a status: ' + result[0].status + ' for org: ' + orgId;
                console.log(msg);
                var responseInfo = $('<p>' + msg + '</p>');
                var divSuccess = $('#divCreateChildOrg').find('.alert-success');
                divSuccess.append(responseInfo);

                $('#create-org-wait').hide();

                //Now show the solutions for the template org    
                getSolutions();
            }
            else {
                // Call back into this same function to check the status again to determine when it changes to Running
                console.log('Success getting agent but status not yet set to Running, calling again...');
                setTimeout(getCloudAgentInfo, 2500);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the agent back for the new org - ' + errorThrown + '</p>');
            var divFailure = $('#divCreateChildOrg').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();

            $('#create-org-wait').hide();
        }
    });
}
