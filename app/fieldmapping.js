/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for calling APIs for retrieving map, entity and field metadata.  Drag/drop handler functions for mapping.
*/



/* Helper function to hide mapping related UI when selections are changed
*/
function resetMapUI() {
    // Clear the list of maps
    $('#divShowMaps').html('');

    // Clear the source and target tables
    $('#divMapInfoSource').slideUp();
    $('#divMapInfoTarget').slideUp();

    // Clear the success/fail information areas
    var divSuccess = $('#divShowFieldMappings').find('.alert-success');
    var divFailure = $('#divShowFieldMappings').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();

    divSuccess = $('#divValidateFieldMapping').find('.alert-success');
    divFailure = $('#divValidateFieldMapping').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();

    $('#divShowValidationResults').slideUp();

    divSuccess = $('#divSaveFieldMapping').find('.alert-success');
    divFailure = $('#divSaveFieldMapping').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();
}



/* Get a list of maps for a solution
*/
function getMaps(prevContainerId) {
    resetMapUI();

    //Set the active solution
    activeSolutionId = $('#' + prevContainerId).find('#selectSolution').val();
    if (!activeSolutionId) {
        alert('You must select a solution.');
        return;
    }

    console.log('Loading Maps for solutionId: ', activeSolutionId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/maps',
        // GET /v1/orgs/{orgId}/solutions/{solutionId}/maps
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log('Retrieved maps for org: ' + orgId + ', solutionId: ' + activeSolutionId);

            // Create an html option for each map entry found.
            var items = $.map(result, function (mapEntry, index) {
                var optionItem = $('<option value="' + mapEntry.id + '">' + mapEntry.name + '</option>')
                return optionItem;
            });

            var selectMap = $('<select id="selectMap" size="4" style="width: 300px;"></select>');
            selectMap.append(items);

            // Show the list of maps
            $('#divShowMaps').html(selectMap);

            // Wire up the click handler for when a map is selected, to then load the source/target fields for the map
            $('#selectMap').on('click', loadSelectedMap);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the maps list for the org ' + orgId + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divShowMapsForSolution').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Loads the Map selected in the UI
*/
function loadSelectedMap() {
    var selectedMapId = $('#selectMap').val();
    if (!selectedMapId) {
        alert('You must select a map in Step 3.');
        return;
    }

    console.log('Showing fields for map: ' + selectedMapId);
    $('#load-srctarg-mapping-wait').show();
    $('#divMapInfoSourceAndTarget').show();

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/maps/' + selectedMapId,
        // GET /v1/orgs/{orgId}/solutions/{solutionId}/maps/{mapId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log('Retrieved map: ' + selectedMapId);
            console.log(result);

            // Set the active map into a global variable
            activeMap = result;
            var msg = 'Retrieved map data for map name: ' + result.name + ' , mapId: ' + selectedMapId;
            console.log(msg);

            // Do some basic error checking to see if the activeMap is valid
            if (activeMap === undefined || activeMap.blocks === undefined || activeMap.blocks.length == 0) {
                $('#load-srctarg-mapping-wait').hide();

                var blockInvalidMsg = "Cannot load map with id: " + selectedMapId + " because it is invalid or does not contain any blocks.";
                console.log(blockInvalidMsg);

                var responseInfo = $('<p>' + blockInvalidMsg + '</p>');
                var divFailure = $('#divShowFieldMappings').find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();
                return;
            }

            // Store the source map block into a global variable
            sourceMapBlock = $.grep(activeMap.blocks, function (e) { return e.blockType === SOURCE_BLOCK_TYPE && e.name === SOURCE_BLOCK_NAME; });
            if (sourceMapBlock.length == 0) {
                console.log('Error finding the source block in mapId: ' + activeMap.id);
                $('#load-srctarg-mapping-wait').hide();

                var responseInfo = $('<p>Error finding the ' + SOURCE_BLOCK_NAME + ' source block in mapId: ' + activeMap.id + '</p>');
                var divFailure = $('#divShowFieldMappings').find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();
                return;
            }
            else {
                console.log("Source map block");
                console.log(sourceMapBlock);
            }

            // Find the map's target UpdateInsert block that has target information (by block type and block name)
            // Store the target map block into a global variable
            targetMapBlock = $.grep(activeMap.blocks, function (e) { return e.blockType === TARGET_BLOCK_TYPE && e.name === TARGET_BLOCK_NAME; });
            if (targetMapBlock.length == 0) {
                console.log('Error finding the target block in mapId: ' + activeMap.id);

                var responseInfo = $('<p>Error finding the ' + TARGET_BLOCK_NAME + ' target block in mapId: ' + activeMap.id + '</p>');
                var divFailure = $('#divShowFieldMappings').find('.alert-danger');
                divFailure.append(responseInfo);
                divFailure.slideDown();
                return;
            }
            else {
                console.log("Target map block");
                console.log(targetMapBlock);
            }


            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divShowFieldMappings').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Now retrieve the source fields for this map and remember how many times we try to load them
            getFieldsRetryCount = 0;
            setTimeout(retrieveSourceFields, 100);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            $('#load-srctarg-mapping-wait').hide();
            var responseInfo = $('<p>Error getting mapId: ' + selectedMapId + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divShowFieldMappings').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Gets the source fields from the source connection for the specified map.  Fills in source fields into html table.
*/
function retrieveSourceFields() {
    console.log('Retrieving source fields for mapId: ' + activeMap.id + ' , connectionId: ' + sourceMapBlock[0].connectionId + ' , block name: ' + sourceMapBlock[0].name +
        ' , agentId: ' + agentId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/connections/' + sourceMapBlock[0].connectionId + '/entities/' + sourceMapBlock[0].entity + '/fields?agentId=' + agentId + '&limit=' + MAX_ROWS_TO_RETURN,
        // GET /v1/orgs/{orgId}/connections/{connectionId}/entities/{sourceEntity}/fields?agentId=
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            var msg = 'Retrieved source ' + sourceMapBlock[0].entity + ' entity fields for connectionId: ' + sourceMapBlock[0].connectionId;
            console.log(msg);

            //  Now build the table html for the source fields
            var sourceRows = $.map(result, function (fieldItem, index) {
                if (fieldItem.name != '') {
                    var cssClass;
                    var cellHtml;
                    var draggable = "true";
                    var tblRow = $('<tr></tr>');

                    // Show alternating row colors
                    if (index % 2)
                        cssClass = "";
                    else
                        cssClass = "alt-row";

                    // Determine if this source field is currently mapped, if so don't let it be draggable
                    var fieldMappingItem = $.grep(targetMapBlock[0].fieldMappings, function (e) { return e.targetFormula === (sourceMapBlock[0].entity + '.' + fieldItem.name); });
                    if (fieldMappingItem !== undefined && fieldMappingItem.length > 0) {
                        draggable = "false";
                    }

                    cellHtml = '<td id="' + sourceMapBlock[0].entity + '.' + fieldItem.name + '" class="' + cssClass + ' col-xs-8" draggable="' + draggable + '" ondragstart="dragSourceField(event)">';
                    // Add a checkbox to show if the field has already been mapped or not
                    if (draggable === "true")
                        cellHtml += '<input id=ck_' + sourceMapBlock[0].entity + '.' + fieldItem.name + ' type="checkbox" disabled="true">';
                    else
                        cellHtml += '<input id=ck_' + sourceMapBlock[0].entity + '.' + fieldItem.name + ' type="checkbox" checked="checked" disabled="true">';

                    cellHtml += '&nbsp;';
                    cellHtml += fieldItem.name;
                    cellHtml += '</td>';

                    $(cellHtml).appendTo(tblRow);

                    $('<td class="' + cssClass + ' col-xs-4">' + normalizeDataType(fieldItem.propertyType, fieldItem.size) + '</td>').appendTo(tblRow);
                    return tblRow;
                }
                else
                    return null;
            });

            // Show the table of org and solution information
            $("#tbl-source-fields tbody").html(sourceRows);
            $('#divMapInfoSource').slideDown();

            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divShowFieldMappings').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Now retrieve the target fields for this map
            getFieldsRetryCount = 0;
            setTimeout(retrieveTargetFields, 100);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            var responseInfo;
            var divFailure = $('#divShowFieldMappings').find('.alert-danger');

            // Retry if we get a 404
            if (xhr.status == 404) {
                var warning = "404 trying to load source field metadata";

                // Increment the retry count
                getFieldsRetryCount++;
                if (getFieldsRetryCount <= 3)
                    warning += ", trying again...";

                console.log(warning);
                var responseInfo = $('<p>' + warning + '</p>');
                divFailure.append(responseInfo);
                divFailure.slideDown();

                // Retry
                if (getFieldsRetryCount <= 3)
                    setTimeout(retrieveSourceFields, 10000);
                else
                    $('#load-srctarg-mapping-wait').hide();
            }
            else {
                $('#load-srctarg-mapping-wait').hide();
                responseInfo = $('<p>Error getting source fields for connectionId: ' + sourceMapBlock[0].connectionId + ' - ' + errorThrown + '</p>');
                divFailure.append(responseInfo);
                divFailure.slideDown();
            }
        }
    });
}

/* Gets the target fields from the target connection for the specified map.  Fills in the target html table to show fields and 
   any existing mappings to the source.
*/
function retrieveTargetFields() {
    console.log('Retrieving target fields for mapId: ' + activeMap.id + ' , connectionId: ' + targetMapBlock[0].connectionId + ' , block name: ' + targetMapBlock[0].name +
        ' , agentId: ' + agentId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/connections/' + targetMapBlock[0].connectionId + '/entities/' + targetMapBlock[0].entity + '/fields?agentId=' + agentId + '&limit=' + MAX_ROWS_TO_RETURN,
        // GET /v1/orgs/{orgId}/connections/{connectionId}/entities/{targetEntity}/fields?agentId=
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            targetFields = result;
            var msg = 'Retrieved target ' + targetMapBlock[0].entity + ' entity fields for connectionId: ' + targetMapBlock[0].connectionId;
            console.log(msg);

            //  Now build the table html for the target fields
            var targetRows = $.map(targetFields, function (fieldItem, index) {
                if (fieldItem.name != '') {
                    var isMapped = false;
                    var isFormula = false;
                    var cssClass;
                    var tblRow = $('<tr></tr>');

                    // Show alternating row colors
                    if (index % 2)
                        cssClass = "";
                    else
                        cssClass = "alt-row";

                    $('<td class="' + cssClass + ' col-xs-4">' + fieldItem.name + '</td>').appendTo(tblRow);
                    $('<td class="' + cssClass + ' col-xs-2">' + normalizeDataType(fieldItem.propertyType, fieldItem.size) + '</td>').appendTo(tblRow);

                    // Show the existing mapping for this target field if present
                    var formulaTD = $('<td></td>');
                    formulaTD.addClass(cssClass);
                    formulaTD.addClass('col-xs-6');
                    formulaTD.attr("id", fieldItem.name);
                    formulaTD.html('&nbsp;');

                    // Set the drag/drop events for each TD
                    formulaTD.attr('ondragenter', 'targetDragEnter(event)');
                    formulaTD.attr('ondragleave', 'targetDragLeave(event)');
                    formulaTD.attr("ondrop", 'dropSourceField(event)');
                    formulaTD.attr("ondragover", 'allowFieldDrop(event)');

                    var fieldMappingItem = $.grep(targetMapBlock[0].fieldMappings, function (e) { return e.targetField === fieldItem.name; });
                    if (fieldMappingItem !== undefined && fieldMappingItem.length > 0)
                        isMapped = true;

                    // Create the span that holds the formula text
                    var spanMappedField = $('<span></span>');
                    spanMappedField.addClass('mappedField');
                    if (isMapped) {
                        // If this is CHOOSEBYVAL formula then display abbreviation instead of entire formula
                        if (fieldMappingItem[0].targetFormula.startsWith(CHOOSE_BY_VAL_FORMULA)) {
                            spanMappedField.html(CHOOSE_BY_VAL_FORMULA + '(...)');
                            // Show tooltip with the entire formula
                            spanMappedField.attr('title', fieldMappingItem[0].targetFormula);
                            isFormula = true;
                        }
                        else
                            spanMappedField.html(fieldMappingItem[0].targetFormula);
                    }

                    // Create and set the formula and delete images
                    var imgDel = $('<img src="img/deletesm.png"></img>')
                    imgDel.addClass('deleteAlignRight');
                    imgDel.on('click', function () {
                        deleteMapping(fieldItem.name, true);
                    });
                    if (!isMapped)
                        imgDel.hide();
                    imgDel.appendTo(spanMappedField);

                    // Set the image to add a function to this unmapped target field
                    var imgFx = $('<img src="img/Fxsm.png"></img>')
                    imgFx.addClass('formulaAlignRight');
                    imgFx.on('click', function () {
                        createOrEditFormula(fieldItem.name);
                    });

                    // Hide this if the field is already mapped, unless it is mapped to a formula, in which
                    // case we want to show the image to allow editing of the formula.
                    if (isMapped && !isFormula)
                        imgFx.hide();
                    imgFx.appendTo(spanMappedField);

                    //Add the span to the table cell
                    spanMappedField.appendTo(formulaTD);

                    //Add the table cell to the row
                    formulaTD.appendTo(tblRow);
                    return tblRow;
                }
                else
                    return null;
            });

            // Show the table of org and solution information
            $("#tbl-target-fields tbody").html(targetRows);
            $('#divMapInfoTarget').slideDown();

            $('#load-srctarg-mapping-wait').hide();
            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divShowFieldMappings').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            var responseInfo;
            var divFailure = $('#divShowFieldMappings').find('.alert-danger');

            // Retry if we get a 404
            if (xhr.status == 404) {
                var warning = "404 trying to load target field metadata";

                // Increment the retry count
                getFieldsRetryCount++;
                if (getFieldsRetryCount <= 3)
                    warning += ", trying again...";

                console.log(warning);
                var responseInfo = $('<p>' + warning + '</p>');
                divFailure.append(responseInfo);
                divFailure.slideDown();

                // Retry
                if (getFieldsRetryCount <= 3)
                    setTimeout(retrieveTargetFields, 10000);
                else
                    $('#load-srctarg-mapping-wait').hide();
            }
            else {
                $('#load-srctarg-mapping-wait').hide();
                responseInfo = $('<p>Error getting target fields for connectionId: ' + targetMapBlock[0].connectionId + ' - ' + errorThrown + '</p>');
                divFailure.append(responseInfo);
                divFailure.slideDown();
            }
        }
    });
}

/* Validate the map
*/
function validateMap() {
    console.log('Validate dirty map changes for mapId: ' + activeMap.id);
    console.log(activeMap);

    // Update the map
    $.ajax({
        type: "POST",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/maps/' + activeMap.id + '/validate',
        // POST /v1/orgs/{orgId}/solutions/{solutionId}/maps/{mapId}/validate
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(activeMap),
        contentType: 'application/json',
        success: function (result) {
            // The response for a validate POST contain a single "value" entry which is the validation ID
            var msg = 'Successfully submitted a validate map request for mapId: ' + activeMap.id + ', now waiting for results, validationId: ' + result.value;
            console.log(msg);
            console.log(result);

            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divValidateFieldMapping').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            $('#validate-mapping-wait').show();
            setTimeout(getValidateMapResults.bind(null, result.value), 1500);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error submitting a validate request for mapId: ' + activeMap.id + ' - ' + xhr.responseText + '</p>');
            var divFailure = $('#divValidateFieldMapping').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
            $('#validate-mapping-wait').hide();
        }
    });
}

/* Get the validation results and show them in a table
*/
function getValidateMapResults(validationId) {
    console.log('Getting validation results for mapId: ' + activeMap.id + ', validationId: ' + validationId);

    $.ajax({
        type: "GET",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/maps/' + validationId + '/validationresults',
        // GET /v1/orgs/{orgId}/solutions/{solutionId}/maps/{validationId}/validationresults
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        success: function (result) {
            console.log('Retrieved validation results for mapId: ' + activeMap.id);
            console.log(result);

            if (result.length > 0) {
                //  Build an html for the validation results
                var sourceRows = $.map(result, function (validationItem, index) {
                    var cssClass;
                    var tblRow = $('<tr></tr>');

                    // Show alternating row colors
                    if (index % 2)
                        cssClass = "";
                    else
                        cssClass = "alt-row";

                    $('<td class="' + cssClass + ' col-xs-2">' + validationItem.targetFieldName + '</td>').appendTo(tblRow);
                    $('<td class="' + cssClass + ' col-xs-1">' + validationItem.severity + '</td>').appendTo(tblRow);
                    $('<td class="' + cssClass + ' col-xs-4">' + validationItem.blockId + '</td>').appendTo(tblRow);
                    $('<td class="' + cssClass + ' col-xs-5">' + validationItem.description + '</td>').appendTo(tblRow);

                    return tblRow;
                });

                // Show the table of org and solution information
                $("#tbl-validation-results tbody").html(sourceRows);
                $('#divShowValidationResults').slideDown();
                msg = "Showing validation warnings/errors for mapId: " + activeMap.id + ', see table below for details:';
            }
            else
                msg = "There weren't any validation warnings or errors for mapId: " + activeMap.id;

            // Show success message
            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divValidateFieldMapping').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();
            $('#validate-mapping-wait').hide();

            // Show the save button so that the user can proceed
            $('#save-map').show();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            var responseInfo = $('<p>Error getting the validation results for mapId: ' + activeMap.id + ' - ' + errorThrown + '</p>');
            var divFailure = $('#divValidateFieldMapping').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
            $('#validate-mapping-wait').hide();
        }
    });


}

/* Save all map changes and prepares the solution.
*/
function saveMap() {
    console.log('Saving map changes for mapId: ' + activeMap.id);
    console.log(activeMap);

    // Update the map
    $.ajax({
        type: "PUT",
        url: APIURL + orgId + '/solutions/' + activeSolutionId + '/maps/advanced/' + activeMap.id,
        // PUT /v1/orgs/{orgId}/solutions/{solutionId}/maps/advanced/{mapId}
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        dateType: 'json',
        data: JSON.stringify(activeMap),
        contentType: 'application/json',
        success: function (result) {
            // Show the success message
            var msg = 'Successfully updated mapId: ' + activeMap.id + ' for org: ' + orgId;
            console.log(msg);
            console.log(result);

            var responseInfo = $('<p>' + msg + '</p>');
            var divSuccess = $('#divSaveFieldMapping').find('.alert-success');
            divSuccess.append(responseInfo);
            divSuccess.slideDown();

            // Now that the map has been updated, prepare the solution again.
            // Note that on the Provision tab after the solution is prepared, the getSolutionPrepareStatus() function
            // is called to repeatedly check for a finished status.  That is not done here for brevity.
            prepareSolution('divSaveFieldMapping');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);

            // Show the error message
            var responseInfo = $('<p>Error updating the map - ' + xhr.responseText + '</p>');
            var divFailure = $('#divSaveFieldMapping').find('.alert-danger');
            divFailure.append(responseInfo);
            divFailure.slideDown();
        }
    });
}

/* Functions for target field manipulation (delete, add a function) */

/* This function is called when the user clicks on the formula icon to create or edit the 
   ChooseByVal formula.  For editing, parses the formula and populates the modal popup fields.
*/
function createOrEditFormula(targetField) {
    console.log('createOrEditFormula', targetField);

    // Clear the array for value/results
    cbv_ValueResults.length = 0;

    // Remember the target field for saving the formula later
    activeTargetField = targetField;

    // Clear the UI
    $('#cbv-expression').val('');
    $('#cbv-default').val('');
    $("#tbl-val-res tbody").empty();

    // Determine if an existing formula is being edited
    var fieldMappingItem = $.grep(targetMapBlock[0].fieldMappings, function (e) { return e.targetField === targetField; });
    if (fieldMappingItem !== undefined && fieldMappingItem.length > 0) {
        console.log('createOrEditFormula - found a mapped field', fieldMappingItem);

        if (fieldMappingItem[0].targetFormula != undefined && fieldMappingItem[0].targetFormula.startsWith(CHOOSE_BY_VAL_FORMULA)) {
            // A formula mapping exists for this target field
            console.log('createOrEditFormula - found existing formula', fieldMappingItem[0].targetFormula);
            // Parse the formula to load expression, value/result pairings, and default (if present)

            // EX: "CHOOSEBYVAL( Lead.billingCountry, \"US\", \"United States\", \"USA\", \"United States\", \"CA\", \"Canada\", Lead.billingCountry)"             
            var fields = fieldMappingItem[0].targetFormula.split(',');
            if (fields.length > 0) {
                // Parse out the expression field
                var expIdx = fields[0].indexOf('(');
                var expression = fields[0].substring(expIdx + 1);
                $('#cbv-expression').val(expression.trim());

                var endIdx;
                if (fields.length % 2 == 0) {
                    // Having an even number of fields means there is an ending default value in the list
                    endIdx = fields.length - 1;
                    $('#cbv-default').val(fields[fields.length - 1].substring(0, fields[fields.length - 1].length - 1).trim());
                }
                else
                    endIdx = fields.length;

                for (i = 1; i < endIdx; i += 2) {
                    var temp1 = fields[i].trim();
                    var temp2 = fields[i + 1].trim();
                    if (fields.length % 2 != 0 && (i + 1 == endIdx - 1)) {
                        // Since there isn't a default value at the end, need to remove ending right paren 
                        // from the final value field
                        var expIdx2 = temp2.indexOf(')');
                        temp2 = temp2.substring(0, expIdx2);
                        temp2 = temp2.trim();
                    }

                    var cbvItem = {};
                    cbvItem.value = temp1.slice(1, temp1.length - 1);
                    cbvItem.result = temp2.slice(1, temp2.length - 1);
                    cbv_ValueResults.push(cbvItem);

                    // Add the table row
                    addCBVValueResultToUI(cbvItem.value, cbvItem.result);
                }
            }
        }
    }

    // Show the modal for creating/editing the formula
    $('#chooseByFormula').modal('show');
}

/* This function gets called each time the user adds another value/result in the ChooseByVal modal popup
   Adds a value/result to the global array
 */
function addCBVValueResult() {

    if ($('#cbv-add-value').val() == '' || $('#cbv-add-result').val() == '') {
        alert('You must enter a valid value and result.');
        return;
    }

    var cbvItem = {};
    cbvItem.value = $('#cbv-add-value').val();
    cbvItem.result = $('#cbv-add-result').val();

    cbv_ValueResults.push(cbvItem);

    // Add the table row
    addCBVValueResultToUI(cbvItem.value, cbvItem.result);

    // Clear the input UI 
    $('#cbv-add-value').val('');
    $('#cbv-add-result').val('');

    console.log('added value result to array:', cbv_ValueResults);
}

/* Helper function to add a value/result table row in the ChooseByVal modal popup
 */
function addCBVValueResultToUI(value, result) {

    // Set the value/result into the UI
    var tblRow = $('<tr id="' + value + '"></tr>');
    $('<td>' + value + '</td>').appendTo(tblRow);
    $('<td>' + result + '</td>').appendTo(tblRow);

    var deleteTD = $('<td></td>');
    // Add the ability to delete this value/result entry
    var imgDelVR = $('<img src="img/deletesm.png"></img>')
    //imgDelVR.addClass('deleteAlignRight');
    imgDelVR.on('click', function () {
        deleteCBVValueResult(value);
    });
    imgDelVR.appendTo(deleteTD);
    deleteTD.appendTo(tblRow);

    tblRow.appendTo($("#tbl-val-res tbody"));
}

/* Called when a value/result row is being deleted by the user.  Removes the entry from the 
   global array and removes the table row in the modal.
*/
function deleteCBVValueResult(valueName) {
    console.log('deleting CBC Value: ', valueName);
    // Find and remove the entry from the value/result array
    var indexes = $.map(cbv_ValueResults, function (obj, index) {
        if (obj.value == valueName) {
            return index;
        }
    });

    if (indexes.length > 0) {
        cbv_ValueResults.splice(indexes[0], 1);
    }
    else {
        console.log('Cannot find value/result item for value: ', valueName);
    }

    // Remove the table row in the modal
    $('table#tbl-val-res tr#' + valueName).remove();
}

/* Build the ChooseByValue formula in proper format and save into the target mapping.  This is a click
   handler for the formula modal popup.
*/
function saveChooseByVal() {
    if ($('#cbv-expression').val() == '') {
        alert('You must enter a valid expression.');
        return;
    }
    else if (cbv_ValueResults.length == 0) {
        alert('You must enter at least one value/result.');
    }

    // Build the formula
    // EX: "CHOOSEBYVAL( Lead.billingCountry, \"US\", \"United States\", \"USA\", \"United States\", \"CA\", \"Canada\", Lead.billingCountry)"

    var formula = CHOOSE_BY_VAL_FORMULA + '(' + $('#cbv-expression').val();
    $.each(cbv_ValueResults, function (index, valResultItem) {
        formula += ', "';
        formula += valResultItem.value;
        formula += '", "';
        formula += valResultItem.result;
        formula += '"';
    });

    if ($('#cbv-default').val() != '') {
        formula += ', ';
        formula += $('#cbv-default').val();
    }

    formula += ')';
    console.log(formula);

    // Add the ChooseByVal formula to the target field mapping
    console.log('Mapping CHOOSEBYVAL formula: ' + formula + ' onto target field: ' + activeTargetField);
    addMappedField(formula, activeTargetField);

    // Now hide the modal
    $('#chooseByFormula').modal('hide');
}

/* Called when a source field is dropped onto a target field.  Add this new mapping into the global 
   targetMapBlock.fieldMappings array, which is the in-memory version of the field mappings.
*/
function addMappedField(sourceOrFormula, targetField) {
    //Set the source entity/field text into the target cell
    var targetTD = $('#' + targetField);

    // Show the delete image and hide the formula image
    var dImg = targetTD.find('img.deleteAlignRight').detach();
    var fImg = targetTD.find('img.formulaAlignRight').detach();
    // Set the source field information or show abbreviated formula
    if (sourceOrFormula.startsWith(CHOOSE_BY_VAL_FORMULA)) {
        targetTD.children('span').html(CHOOSE_BY_VAL_FORMULA + '(...)');
        // Show tooltip with the entire formula
        targetTD.children('span').attr('title', sourceOrFormula);
        // Keep showing the formula image for editing
        fImg.show();
    }
    else {
        targetTD.children('span').html(sourceOrFormula);
        fImg.hide();
    }

    targetTD.children('span').append(dImg);
    targetTD.children('span').append(fImg);

    // Hide the delete
    dImg.show();


    // Find the target field to get the data type
    var targetFieldItem = $.grep(targetFields, function (e) { return e.name === targetField; });
    console.log(targetFieldItem);
    if (targetFieldItem === undefined) {
        console.log("addMappedField: cannot find the target field: " + targetField);
        return;
    }

    // Delete the existing mapping if it already exists (which will be
    // the case when editing a formula)
    deleteMapping(targetField, false);

    // Save back to the target block of the map
    var mapping = {};
    mapping.targetDataType = targetFieldItem[0].propertyType;
    mapping.targetField = targetFieldItem[0].name;
    mapping.targetFormula = sourceOrFormula;

    targetMapBlock[0].fieldMappings.push(mapping);
}

/* Handler for user clicking on delete icon.  Unmaps the source from the target and 
   changes the image to show the formula icon.
   id - targetField ID
   clearUI - if true then call the function to reset the UI
*/
function deleteMapping(id, clearUI) {
    console.log('deleteMapping', id);
    console.log('fieldMappings before', targetMapBlock[0].fieldMappings);

    // Find and remove the mapping from the target map block field mappings
    var indexes = $.map(targetMapBlock[0].fieldMappings, function (obj, index) {
        if (obj.targetField == id) {
            return index;
        }
    });

    if (indexes.length > 0) {
        targetMapBlock[0].fieldMappings.splice(indexes[0], 1);
    }
    else {
        console.log('Cannot find field mapping for target field: ', id);
    }

    // Clear the target and source fields in the UI
    if (clearUI)
        unMapSourceTargetUI(id);

    console.log('fieldMappings after', targetMapBlock[0].fieldMappings);
}

/* Remove all field mappings from the map being edited (handler for the delete all icon)
*/
function deleteAllMappings() {
    if (!confirm('Remove all field mappings?'))
        return;

    console.log('Removing all field mappings for mapId: ', activeMap.id);

    var targetRows = $.map(targetFields, function (fieldItem, index) {
        if (fieldItem.name != '') {
            // Determine if this target is currently mapped by looking it up in the global field mappings array
            var fieldMappingItem = $.grep(targetMapBlock[0].fieldMappings, function (e) { return e.targetField === fieldItem.name; });
            if (fieldMappingItem !== undefined && fieldMappingItem.length > 0) {
                unMapSourceTargetUI(fieldItem.name);
            }
        }
    });

    // Now clear the global array of existing mappings
    targetMapBlock[0].fieldMappings.length = 0;
}

/* Utility function to unmap the source from the target in the UI
*/
function unMapSourceTargetUI(targetId) {
    console.log('Unmap source from target (UI adjustment), target id: ', targetId);

    var targetTD = $('#' + targetId);

    // Uncheck the source and make it draggable again
    var sourceID = targetTD.children('span').text();
    if (!sourceID.startsWith(CHOOSE_BY_VAL_FORMULA))
        flipSourceFieldDragState(sourceID, true);

    // Remove the source field text from the span within the target table cell
    var dImg = targetTD.find('img.deleteAlignRight').detach();
    var fImg = targetTD.find('img.formulaAlignRight').detach();
    targetTD.children('span').html('');
    targetTD.children('span').append(dImg);
    targetTD.children('span').append(fImg);

    // Hide the delete icon and show the formula icon
    dImg.hide();
    fImg.show();
}


/* Drag Drop handlers for dragging source field to target field */


/* Called when a source field is dropped onto a target field.  Add this new mapping into the target block.
*/
function dropSourceField(ev) {
    ev.preventDefault();

    ev.target.style.border = "";
    // Get the full source field name (format is entity.fieldname)
    var data = ev.dataTransfer.getData("fieldName");
    console.log('Dropping source field: ' + data + ' onto target field: ' + ev.target.id);

    // Map the source field into the target.  This includes UI changes.
    addMappedField(data, ev.target.id);

    // Now that the field has been dropped, make it non-draggable on the source side to prevent being able to drag/drop it again
    flipSourceFieldDragState(data, false);
}

function dragSourceField(ev) {
    ev.dataTransfer.setData("fieldName", ev.target.id);
}

function allowFieldDrop(ev) {
    ev.preventDefault();
}

function targetDragEnter(ev) {
    ev.target.style.border = "2px dotted red";
}

function targetDragLeave(ev) {
    ev.target.style.border = "";
}

/* Helper function to check or uncheck the source field checkbox
*/
function flipSourceFieldDragState(sourceFieldId, draggable) {
    console.log('Making source field: ' + sourceFieldId + ' draggable=' + draggable);
    //Escape the period in the table cell id value before using jquery to select it
    var tdID = sourceFieldId.replace(".", "\\.");
    $('#' + tdID).attr("draggable", draggable);
    // Set the checkbox to checked if not draggable
    var tdCheck = 'ck_' + sourceFieldId.replace(".", "\\.");

    if (draggable)
        $('#' + tdCheck).removeAttr('checked');
    else
        $('#' + tdCheck).attr("checked", "checked");
}
/* End drag/drop handlers */


